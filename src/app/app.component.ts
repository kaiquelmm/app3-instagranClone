import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app3';

  ngOnInit(): void {

    const config = {
      apiKey: 'AIzaSyCjwMfG1XFHxxdj_9FlCFO933ACeR4mxfA',
      authDomain: 'jta-instagram-clone-912c0.firebaseapp.com',
      databaseURL: 'https://jta-instagram-clone-912c0.firebaseio.com',
      projectId: 'jta-instagram-clone-912c0',
      storageBucket: 'jta-instagram-clone-912c0.appspot.com',
      messagingSenderId: '228330993998'
    };

    firebase.initializeApp(config);
  }
}
